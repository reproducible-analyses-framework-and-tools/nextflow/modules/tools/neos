#!/usr/bin/env nextflow

// Fusion
include { fusions_to_fusion_cds_fastas } from '../seq_variation/seq_variation.nf'
include { lenstools_make_fusion_peptides_context } from '../lenstools/lenstools.nf'

// CTA/Self
include { lenstools_filter_expressed_self_genes } from '../lenstools/lenstools.nf'
include { lenstools_get_expressed_selfs_bed } from '../lenstools/lenstools.nf'
include { lenstools_make_self_antigen_peptides } from '../lenstools/lenstools.nf'

// ERV
include { lenstools_filter_expressed_ervs } from '../lenstools/lenstools.nf'
include { lenstools_filter_expressed_ervs_without_control } from '../lenstools/lenstools.nf'
include { samtools_coverage as samtools_coverage_erv} from  '../samtools/samtools.nf'
include { lenstools_get_expressed_ervs_bed } from '../lenstools/lenstools.nf'
include { bcftools_simple_germline_consensus as bcftools_simple_germline_consensus_erv } from '../bcftools/bcftools.nf'
include { lenstools_filter_ervs_by_rna_coverage } from '../lenstools/lenstools.nf'
include { lenstools_make_erv_peptides } from '../lenstools/lenstools.nf'

// SNV/InDel
include { lenstools_filter_expressed_variants } from '../lenstools/lenstools.nf'
include { tx_list_to_norm_and_tumor_cds_fastas } from '../seq_variation/seq_variation.nf'
include { lenstools_make_snv_peptides_context } from '../lenstools/lenstools.nf'
include { lenstools_make_indel_peptides_context } from '../lenstools/lenstools.nf'

// Viral
include { samtools_index as samtools_index_viral } from '../samtools/samtools.nf'
include { samtools_sort as samtools_sort_viral } from '../samtools/samtools.nf'
include { lenstools_filter_expressed_viruses } from '../lenstools/lenstools.nf'
include { samtools_coverage as samtools_coverage_viral} from  '../samtools/samtools.nf'
include { lenstools_filter_viruses_by_rna_coverage } from '../lenstools/lenstools.nf'
include { lenstools_get_expressed_viral_bed } from '../lenstools/lenstools.nf'
include { bcftools_simple_germline_consensus as bcftools_simple_germline_consensus_viral } from '../bcftools/bcftools.nf'
include { lenstools_make_viral_peptides } from '../lenstools/lenstools.nf'

// Misc
include { bcftools_consensus_germline } from '../bcftools/bcftools.nf'
include { htslib_bgzip } from '../htslib/htslib.nf'
include { bcftools_index } from '../bcftools/bcftools.nf'
include { samtools_faidx_fetch } from '../samtools/samtools.nf'


workflow som_vars_to_neos {
// Runs filtered (e.g. expressed and isolated) SNVs through NetMHCpan and other
// downstream tools.
//
// input:
// output:
//
// require:
//   VCFS_AND_INDICES
//   CLASS1_ALLELES
  take:
    som_vars_vcfs
    phased_tumor_vcfs
    phased_germline_vcfs
    quants
    gtf
    dna_ref
    pep_ref
    som_var_type
    lenstools_filter_expressed_variants_parameters
    bcftools_index_phased_germline_parameters
    bcftools_index_phased_tumor_parameters
    lenstools_get_expressed_transcripts_bed_parameters
    samtools_faidx_fetch_somatic_folder_parameters
  main:
    som_vars_vcfs
      .combine(quants, by: [0])
      .map{ [it[0], it[1], it[2], it[3], it[4], it[7]] }
      .set{ annot_vars_and_exp }
    lenstools_filter_expressed_variants(
      annot_vars_and_exp,
      lenstools_filter_expressed_variants_parameters)
    tx_list_to_norm_and_tumor_cds_fastas(
      som_vars_vcfs,
      lenstools_filter_expressed_variants.out.somatic_transcripts,
      gtf,
      dna_ref,
      phased_germline_vcfs,
      phased_tumor_vcfs,
      bcftools_index_phased_germline_parameters,
      bcftools_index_phased_tumor_parameters,
      lenstools_get_expressed_transcripts_bed_parameters,
      samtools_faidx_fetch_somatic_folder_parameters)
    tx_list_to_norm_and_tumor_cds_fastas.out.somatic_vcfs_w_var_tx_seqs
      .join(lenstools_filter_expressed_variants.out.somatic_transcripts, by: [0, 1, 2, 3])
      .set{ somatic_vcfs_w_var_tx_seqs_w_expressed_txs }
    if( som_var_type =~ /SNV|snv/ ) {
      lenstools_make_snv_peptides_context(
        somatic_vcfs_w_var_tx_seqs_w_expressed_txs,
        gtf,
        pep_ref)
      lenstools_make_snv_peptides_context.out.mutant_fastas
        .set{ mutant_fastas }
      lenstools_make_snv_peptides_context.out.nt_fastas
        .set{ mutant_nt_fastas }
    } else if( som_var_type =~ /INDEL|indel|InDel/ ) {
      lenstools_make_indel_peptides_context(
        somatic_vcfs_w_var_tx_seqs_w_expressed_txs,
        gtf,
        pep_ref)
      lenstools_make_indel_peptides_context.out.peptide_fastas
        .set{ mutant_fastas }
      lenstools_make_indel_peptides_context.out.nt_fastas
        .set{ mutant_nt_fastas }
    }
 emit:
   som_var_c1_peptides = mutant_fastas
   som_var_c1_nts = mutant_nt_fastas
}


workflow fusions_to_neos {
// Runs filtered (e.g. expressed and isolated) SNVs through netMHCpan and other
// downstream tools.
//
// input:
// output:
//
// require:
//   VCFS_AND_INDICES
//   CLASS1_ALLELES
  take:
    fusions
    phased_germline_vcfs
    gtf
    dna_ref
    bcftools_index_phased_germline_parameters
    lenstools_get_fusion_transcripts_bed_parameters
    samtools_faidx_fetch_parameters
  main:
    fusions_to_fusion_cds_fastas(
      fusions,
      gtf,
      dna_ref,
      phased_germline_vcfs,
      bcftools_index_phased_germline_parameters,
      lenstools_get_fusion_transcripts_bed_parameters,
      samtools_faidx_fetch_parameters)
    fusions
      .join(fusions_to_fusion_cds_fastas.out.fusion_tx_exons, by:[0, 1])
      .map{ [it[0], it[1], it[4], it[2], it[3], it[6]] }.set{ fusions_w_tx_exons }
    lenstools_make_fusion_peptides_context(
      fusions_w_tx_exons,
      gtf)
 emit:
   fusion_c1_peptides = lenstools_make_fusion_peptides_context.out.fusion_peptides
   fusion_c1_nts = lenstools_make_fusion_peptides_context.out.fusion_nts
}


workflow ervs_to_neos {
// Runs filtered (e.g. expressed and isolated) InDels through netMHCpan and
// other downstream tools.
//
// input:
// output:
//
// require:
//   EXPRESSED_HERVS
//   CLASS1_ALLELES
  take:
    dna_ref
    rna_tumor_bams
    rna_tumor_tx_bams
    quants
    geve_general_ref
    lenstools_get_expressed_ervs_bed_parameters
    lenstools_make_erv_peptides_parameters
    lenstools_filter_expressed_ervs_parameters
    lenstools_filter_ervs_by_rna_coverage_parameters
    normal_control_quant
    tpm_threshold
  main:
    samtools_coverage_erv(
      rna_tumor_tx_bams.map{ [it[0], it[1], it[2], it[3]] },
      '')
    if( normal_control_quant ) {
      lenstools_filter_expressed_ervs(
        quants.filter{ it[1] =~ 'ar-' },
        normal_control_quant,
        lenstools_filter_expressed_ervs_parameters)
      lenstools_filter_ervs_by_rna_coverage(
        lenstools_filter_expressed_ervs.out.expressed_ervs
          .join(samtools_coverage_erv.out.bam_coverage, by: [0, 1, 2]),
        lenstools_filter_ervs_by_rna_coverage_parameters)
    // Otherwise, if a normal sample is included in the manifest...
    } else if( tpm_threshold ) {
      lenstools_filter_expressed_ervs_without_control(
        quants.filter{ it[1] =~ 'ar-' },
        tpm_threshold,
        lenstools_filter_expressed_ervs_parameters)
      lenstools_filter_ervs_by_rna_coverage(
        lenstools_filter_expressed_ervs_without_control.out.expressed_ervs
          .join(samtools_coverage_erv.out.bam_coverage, by: [0, 1, 2]),
        lenstools_filter_ervs_by_rna_coverage_parameters)
    } else {
      lenstools_filter_expressed_ervs(
        quants.filter{ it[1] =~ 'ar-' },
        quants.filter{ it[1] =~ 'nr-' }.map{ it[3] }.first(),
        lenstools_filter_expressed_ervs_parameters)
      lenstools_filter_ervs_by_rna_coverage(
        lenstools_filter_expressed_ervs.out.expressed_ervs
          .join(samtools_coverage_erv.out.bam_coverage, by: [0, 1, 2]),
        lenstools_filter_ervs_by_rna_coverage_parameters)
    }

    lenstools_get_expressed_ervs_bed(
      lenstools_filter_ervs_by_rna_coverage.out.covered_ervs,
      geve_general_ref,
      lenstools_get_expressed_ervs_bed_parameters)
    bcftools_simple_germline_consensus_erv(
        rna_tumor_bams,
        dna_ref,
        lenstools_get_expressed_ervs_bed.out.expressed_ervs_beds)
    lenstools_filter_ervs_by_rna_coverage.out.covered_ervs
      .join(bcftools_simple_germline_consensus_erv.out.consensus_fastas, by:[0, 1, 2])
      .set{ ervs_and_fastas }
    lenstools_make_erv_peptides(
      ervs_and_fastas,
      geve_general_ref,
      lenstools_make_erv_peptides_parameters)
 emit:
   erv_c1_peptides = lenstools_make_erv_peptides.out.erv_peptides
   erv_c1_nts = lenstools_make_erv_peptides.out.erv_nts
}


workflow selfs_to_neos {
// Runs filtered (e.g. expressed and isolated) InDels through netMHCpan and
// other downstream tools.
//
// input:
// output:
//
// require:
//   EXPRESSED_SELFS
//   CLASS1_ALLELES
  take:
    quants
    vcfs
    gtf
    dna_ref
    cta_self_gene_list
    samtools_index_parameters
    lenstools_filter_expressed_self_parameters
    lenstools_get_expressed_selfs_bed_parameters
    samtools_faidx_fetch_parameters
    bcftools_index_parameters
  main:
    lenstools_filter_expressed_self_genes(
      quants.filter{ it[1] =~ 'ar-' },
      gtf,
      cta_self_gene_list,
      lenstools_filter_expressed_self_parameters)
    lenstools_get_expressed_selfs_bed(
      lenstools_filter_expressed_self_genes.out.expressed_selfs,
      gtf,
      lenstools_get_expressed_selfs_bed_parameters)
    samtools_faidx_fetch(
      lenstools_get_expressed_selfs_bed.out.expressed_selfs_beds,
      dna_ref,
      'expressed_selfs',
      samtools_faidx_fetch_parameters)
//  The below block should be done once _prior_ to distributing the VCFs to
//  individual workflows
    htslib_bgzip(
      vcfs.map{ [it[0], it[2], it[3], it[4]] })
    bcftools_index(
      htslib_bgzip.out.bgzip_files,
      bcftools_index_parameters)
//  Using tumor sample ID from tumor variants file when joining channels.
    bcftools_index.out.vcfs_w_csis
      .join(samtools_faidx_fetch.out.fetched_fastas, by: [0, 2])
      .map{ [it[0], it[5], it[1], it[3], it[4], it[6]] }
      .set{ vcfs_w_csis_w_refs }
    bcftools_index.out.vcfs_w_csis
      .join(samtools_faidx_fetch.out.fetched_fastas, by: [0, 2], remainder: true)
      .filter{ it[2] =~ /null/ }
      .map{ [it[0], it[3], it[1], it[4]] }
      .set{ rna_only_samps }
    bcftools_consensus_germline(
      vcfs_w_csis_w_refs,
      '-H R')
    bcftools_consensus_germline.out.consensus_fastas
      .concat(rna_only_samps)
      .set{ all_fastas }
    all_fastas
      .join(lenstools_filter_expressed_self_genes.out.expressed_selfs, by: [0, 1, 2])
      .set{ consensus_fastas_and_expressed_selfs }
    lenstools_make_self_antigen_peptides(
      consensus_fastas_and_expressed_selfs,
      gtf)
 emit:
   self_antigen_c1_peptides = lenstools_make_self_antigen_peptides.out.self_antigen_peptides
   self_antigen_c1_nts = lenstools_make_self_antigen_peptides.out.self_antigen_nts
}


workflow viruses_to_neos {
// Runs filtered (e.g. expressed and isolated) InDels through netMHCpan and
// other downstream tools.
//
// input:
// output:
//
// require:
//   VCFS_AND_INDICES
//   CLASS1_ALLELES
  take:
    viral_cds_counts
    viral_bams
    viral_cds_ref
    lenstools_filter_expressed_viruses_parameters
    lenstools_filter_viruses_by_rna_coverage_parameters
    lenstools_get_expressed_viral_bed_parameters
    lenstools_make_viral_peptides_parameters
  main:
    //The first step filters by non-zero counts from VirDetect workflow. The
    //second step filters on coverage.
    lenstools_filter_expressed_viruses(
      viral_cds_counts,
      viral_cds_ref,
      lenstools_filter_expressed_viruses_parameters)
    samtools_sort_viral(
      viral_bams,
      '')
    samtools_index_viral(
      samtools_sort_viral.out.bams,
      '')
    samtools_coverage_viral(
      samtools_sort_viral.out.bams,
      '')
    lenstools_filter_viruses_by_rna_coverage(
      lenstools_filter_expressed_viruses.out.expressed_viruses
        .join(samtools_coverage_viral.out.bam_coverage, by: [0, 1, 2]),
      lenstools_filter_viruses_by_rna_coverage_parameters)
    lenstools_get_expressed_viral_bed(
      lenstools_filter_viruses_by_rna_coverage.out.covered_viruses,
      viral_cds_ref,
      lenstools_get_expressed_viral_bed_parameters)
    bcftools_simple_germline_consensus_viral(
      samtools_index_viral.out.bams_and_bais,
      viral_cds_ref,
      lenstools_get_expressed_viral_bed.out.expressed_viral_beds)
    lenstools_make_viral_peptides(
      bcftools_simple_germline_consensus_viral.out.consensus_fastas,
      lenstools_make_viral_peptides_parameters)
 emit:
   viral_consensus_fastas = bcftools_simple_germline_consensus_viral.out.consensus_fastas
   viral_bams_and_bais = samtools_index_viral.out.bams_and_bais
   viral_c1_peptides = lenstools_make_viral_peptides.out.viral_peptides
   viral_c1_nts = bcftools_simple_germline_consensus_viral.out.consensus_fastas
}
